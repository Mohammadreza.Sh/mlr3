mlr3 for geospatial data
========================

#### Overview

This tutorial shows step by step how to:

-   Prepare point and gridded data for spatial predictions
-   Fit models using mlr3
-   Finetune important models such as ranger
-   Run feature selection and reduce number of covariates
-   Produce Ensemble models (stacked learners) using the 5-fold spatial
    cross-validation
-   Generate predictions and produce complete maps

#### installing required packages

First we need to install required packages and load them into session
where is needed.

    ls <- c("rgdal", "raster", "plotKML", "ranger", "mlr3", "glmnet", "knitr", "stringr", "checkpoint","sp","ggplot2","sf","mlr3spatiotempcv","mlr3learners","mlr3pipelines","mlr3filters","mlr3tuning","paradox","ggmap","mltools")

    new.packages <- ls[!(ls %in% installed.packages()[,"Package"])]
    if(length(new.packages)) install.packages(new.packages)

#### Datasets

The data we are going to work on are:

-   meuse data set; target = zinc,
-   SIC97 data set; target = daily rainfall,
-   edgeroi data set; target = 3D soil carbon.

First we process meuse data set and explain whole steps in detail and
later we apply what we learned on the other two data sets.

    library("sp")
    data(meuse)
    class(meuse)

    ## [1] "data.frame"

    meuse <- na.omit(meuse)

#### Visualization

Here wee visualize the data using ggmap package

-   For visualization we need to transform x&y to lat&long and then we
    map the dataset using qmplot()

<!-- -->

    library("proj4")
    library("ggmap")

    ## Loading required package: ggplot2

    ## Google's Terms of Service: https://cloud.google.com/maps-platform/terms/.

    ## Please cite ggmap if you use it! See citation("ggmap") for details.

    proj4string <- "+proj=lcc +lat_1=40.66666666666666 +lat_2=41.03333333333333 +lat_0=40.16666666666666 +lon_0=-74 +x_0=300000 +y_0=0 +datum=NAD83 +units=us-ft +no_defs"
    xy <- data.frame(x=meuse$x, meuse$y)
    pj <- project(xy, proj4string, inverse=TRUE)
    latlon <- data.frame(lat=pj$y, lon=pj$x)
    meuse$lat <- latlon$lat
    meuse$long <- latlon$lon
    qmplot(long, lat, maptype = "watercolor", color = log(zinc),
           data = meuse) +
      scale_colour_viridis_c()

    ## Using zoom = 17...

    ## Source : http://tile.stamen.com/terrain/17/37529/49119.png

    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37529/49119.png.
    ## Source : http://tile.stamen.com/terrain/17/37530/49119.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37530/49119.png.
    ## Source : http://tile.stamen.com/terrain/17/37531/49119.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37531/49119.png.
    ## Source : http://tile.stamen.com/terrain/17/37532/49119.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37532/49119.png.
    ## Source : http://tile.stamen.com/terrain/17/37533/49119.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37533/49119.png.
    ## Source : http://tile.stamen.com/terrain/17/37529/49120.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37529/49120.png.
    ## Source : http://tile.stamen.com/terrain/17/37530/49120.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37530/49120.png.
    ## Source : http://tile.stamen.com/terrain/17/37531/49120.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37531/49120.png.
    ## Source : http://tile.stamen.com/terrain/17/37532/49120.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37532/49120.png.
    ## Source : http://tile.stamen.com/terrain/17/37533/49120.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37533/49120.png.
    ## Source : http://tile.stamen.com/terrain/17/37529/49121.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37529/49121.png.
    ## Source : http://tile.stamen.com/terrain/17/37530/49121.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37530/49121.png.
    ## Source : http://tile.stamen.com/terrain/17/37531/49121.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37531/49121.png.
    ## Source : http://tile.stamen.com/terrain/17/37532/49121.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37532/49121.png.
    ## Source : http://tile.stamen.com/terrain/17/37533/49121.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37533/49121.png.
    ## Source : http://tile.stamen.com/terrain/17/37529/49122.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37529/49122.png.
    ## Source : http://tile.stamen.com/terrain/17/37530/49122.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37530/49122.png.
    ## Source : http://tile.stamen.com/terrain/17/37531/49122.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37531/49122.png.
    ## Source : http://tile.stamen.com/terrain/17/37532/49122.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37532/49122.png.
    ## Source : http://tile.stamen.com/terrain/17/37533/49122.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37533/49122.png.
    ## Source : http://tile.stamen.com/terrain/17/37529/49123.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37529/49123.png.
    ## Source : http://tile.stamen.com/terrain/17/37530/49123.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37530/49123.png.
    ## Source : http://tile.stamen.com/terrain/17/37531/49123.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37531/49123.png.
    ## Source : http://tile.stamen.com/terrain/17/37532/49123.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37532/49123.png.
    ## Source : http://tile.stamen.com/terrain/17/37533/49123.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37533/49123.png.
    ## Source : http://tile.stamen.com/terrain/17/37529/49124.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37529/49124.png.
    ## Source : http://tile.stamen.com/terrain/17/37530/49124.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37530/49124.png.
    ## Source : http://tile.stamen.com/terrain/17/37531/49124.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37531/49124.png.
    ## Source : http://tile.stamen.com/terrain/17/37532/49124.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37532/49124.png.
    ## Source : http://tile.stamen.com/terrain/17/37533/49124.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37533/49124.png.
    ## Source : http://tile.stamen.com/terrain/17/37529/49125.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37529/49125.png.
    ## Source : http://tile.stamen.com/terrain/17/37530/49125.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37530/49125.png.
    ## Source : http://tile.stamen.com/terrain/17/37531/49125.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37531/49125.png.
    ## Source : http://tile.stamen.com/terrain/17/37532/49125.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37532/49125.png.
    ## Source : http://tile.stamen.com/terrain/17/37533/49125.png
    ## Not Found (HTTP 404). Failed to aquire tile /terrain/17/37533/49125.png.

![](mlr3_files/figure-markdown_strict/unnamed-chunk-3-1.png)

We also visualize the “meuse.grid” data as they are spatially same area
as meuse dataset

    data(meuse.grid)
    coordinates(meuse.grid) = ~x+y
    proj4string(meuse.grid) <- CRS("+init=epsg:28992")
    gridded(meuse.grid) = TRUE
    spplot(meuse.grid)

![](mlr3_files/figure-markdown_strict/unnamed-chunk-4-1.png)

#### mlr3 ecosystem

Now it is a good time to jump to mlr3 ecosystem! -
[mlr3](https://mlr3.mlr-org.com/) uses the R6 class system. Some things
may seem unusual if you see them for the first time!

-   Objects are created using “<Class>$new()”
-   Objects have methods that are called like functions
    “task$filter(rows = 1:10)”

we need to set a “task” before any processing for our data set

-   we create “tsk\_meuse” and set “zinc” as our target var

<!-- -->

    library("mlr3")
    tsk_meuse = TaskRegr$new(id = "meuse", backend = meuse, target = "zinc")
    mlr_tasks$add("tsk_meuse", tsk_meuse) 
    tsk_meuse = tsk("tsk_meuse")

#### ML workflow

-   First we run a simple ranger model with a normal 5 fold repeated\_cv
    via a pipeline. The procedure is as following:
    -   scaling data between 0~1
    -   impute mean for NA values (if any)
    -   feature selection
    -   PCA
    -   Defining the learner (here ranger)
    -   Plot the pipeline
    -   promote the pipeline to a graph structure to make use of all
        advantage of mlr3 ecosystem
    -   training the model
    -   make prediction on oob

but waht is mlr PipeLines?

MLR Pipelines
=============

MLR Pipelines is basically a machine Learning Work flows is as the
following:

-   Preprocessing:
    -   Feature extraction, feature selection, missing data imputation,.
        . .
-   Ensemble methods:
    -   Model averaging, model stacking
-   mlr3:modular model fitting

So we are safe to say that mlr3pipelines is a modular ML work flows

PipeOp; MACHINE LEARNING WORKFLOWS
----------------------------------

-   How do they look like:
    -   Building blocks are “PipeOp”
    -   Structure/design of work flow is a “Graph”

THE BUILDING BLOCKS:
====================

PipeOp: Single Unit of Data Operation

-   pip = po(“scale”) to construct
-   pip$train() to process data and create pip state
-   pip$predict() to process data depending on the pip state
-   Multiple inputs or multiple outputs
    -   scaling
    -   PCA
    -   copyfeature union

<!-- -->

    library("mlr3pipelines")
    library("mlr3filters")
    library("mlr3tuning")
    library("mlr3learners")
    graph_pp = mlr_pipeops$get("scale") %>>% 
      mlr_pipeops$get("imputemean") %>>% 
      mlr_pipeops$get("select") %>>% 
      mlr_pipeops$get("pca") %>>%  
      mlr_pipeops$get("learner",
                      learner = mlr_learners$get("regr.ranger"))
    graph_pp$plot()

![](mlr3_files/figure-markdown_strict/unnamed-chunk-6-1.png)

    graph_pp$print()

    ## Graph with 5 PipeOps:
    ##           ID         State    sccssors  prdcssors
    ##        scale <<UNTRAINED>>  imputemean           
    ##   imputemean <<UNTRAINED>>      select      scale
    ##       select <<UNTRAINED>>         pca imputemean
    ##          pca <<UNTRAINED>> regr.ranger     select
    ##  regr.ranger <<UNTRAINED>>                    pca

    glrn = GraphLearner$new(graph_pp)
    glrn$train(tsk_meuse)
    glrn$predict(tsk_meuse) 

    ## <PredictionRegr> for 152 observations:
    ##     row_id truth  response
    ##          1  1022 1006.4777
    ##          2  1141  999.8141
    ##          3   640  699.9767
    ## ---                       
    ##        150   342  333.7375
    ##        151   162  186.5553
    ##        152   375  428.0810

    glrn$model$regr.ranger$model

    ## Ranger result
    ## 
    ## Call:
    ##  ranger::ranger(dependent.variable.name = task$target_names, data = task$data(),      case.weights = task$weights$weight) 
    ## 
    ## Type:                             Regression 
    ## Number of trees:                  500 
    ## Sample size:                      152 
    ## Number of independent variables:  15 
    ## Mtry:                             3 
    ## Target node size:                 5 
    ## Variable importance mode:         none 
    ## Splitrule:                        variance 
    ## OOB prediction error (MSE):       16332.26 
    ## R squared (OOB):                  0.8781144

#### Accuracy plot

Let’s have a look at accuracy plot

    #accuracy plot
    library("hexbin")
    library("lattice")
    library("yardstick")

    ## For binary classification, the first factor level is assumed to be the event.
    ## Use the argument `event_level = "second"` to alter this as needed.

    pfun <- function(x,y, ...){
      panel.xyplot(x, y, ...)
      panel.hexbinplot(x,y, ...)  
      panel.abline(lm(y ~ x), col="black", size = 0.25, lwd = 2)
    }
    palet=colorRampPalette(c("wheat2","yellow" ,"red","red3","orchid","orchid4") )
    xx = glrn$model$regr.ranger$model$predictions
    yy = meuse$zinc
    mixx <- list(xx,yy)
    acc_plt = as.data.frame(mixx)
    RMSE <- signif(rmse(acc_plt, xx, yy)$.estimate, digits=3)
    RSQ <- round(glrn$model$regr.ranger$model$r.squared, digits = 3)
    prepfun <- function(x, y, ...){
      prepanel.hexbinplot(x,y, ...)
    } 
    plt <- hexbinplot(xx ~ yy, mincnt = 1,xbins=70,xlab="measured",ylab="predicted ",inner=0.2, cex.labels=1, colramp= palet, aspect = 1, main=paste0('RMSE: ',RMSE, '    RSQ(oob): ', RSQ), colorcut=c(0,0.01,0.03,0.07,0.15,0.25,0.5,0.75,1), type="g",panel = pfun) 
    plt

![](mlr3_files/figure-markdown_strict/unnamed-chunk-7-1.png)

##### AutoML: Automatic Machine Learning

The idea of AutoML is to let the algorithm make decisions about:

-   what learner to use,
-   what preprocessing to use, and
-   what hyperparameters to use.

1.  and (2) are decisions about graph structure in mlr3pipelines For
    AutoML we need to design tuning procedure

### Tuning

reading necessary packages

    library("mlr3")
    library("mlr3pipelines")
    library("paradox")
    library("mlr3learners")
    library("mlr3tuning")
    library("mlr3filters")
    library("bbotk")
    library("ggplot2")

So first we design the structure of pipeline for ensemble machine
learning

    p1 = ppl("branch", list(
    "pca" = po("pca"),
    "nothing" = po("nop")))
    mlr3filters::Filter -> FilterAnova
    p2 = flt("anova")
    #p2$calculate(task_imputed)$scores
    p3 = ppl("branch", list(
    "knn" = lrn("regr.kknn", id = "knn"),
    "rpt" = lrn("regr.rpart", id = "rpt"),
    "rf" = lrn("regr.ranger", id = "rf")
    ), prefix_branchops = "lrn_")
    gr = p1 %>>% p2 %>>% p3
    glrn = GraphLearner$new(gr)

and then we do PIPELINES TUNING - We need to know the range and
parameters of the learners of interest

    library(mlr3fselect)
    library(mlr3filters)
    library(mlr3learners)
    library("bbotk")
    ps = ParamSet$new(list(
    ParamFct$new("branch.selection", levels = c("pca", "nothing")),
    ParamDbl$new("anova.filter.frac", lower = 0.1, upper = 1),
    ParamFct$new("lrn_branch.selection", levels = c("knn", "rpt", "rf")),
    ParamInt$new("rf.mtry", lower = 1L, upper = 4L),
    ParamDbl$new("rpt.cp", lower = 0, upper =1),
    ParamInt$new("knn.k", lower = 1, upper = 2),
    ParamDbl$new("knn.distance", lower = 1, upper = 2)))
    ps$add_dep("rf.mtry", "lrn_branch.selection", CondEqual$new("rf"))
    ps$add_dep("rpt.cp", "lrn_branch.selection", CondEqual$new("rpt"))
    ps$add_dep("knn.k", "lrn_branch.selection", CondEqual$new("knn"))
    ps$add_dep("knn.distance", "lrn_branch.selection", CondEqual$new("knn"))
    inst = TuningInstanceSingleCrit$new(tsk("tsk_meuse"), glrn, rsmp("cv", folds=5),
    msr("regr.rmse"), ps, trm("evals", n_evals = 5))
    tnr("random_search")$optimize(inst)

    ## INFO  [13:00:45.359] Starting to optimize 7 parameter(s) with '<OptimizerRandomSearch>' and '<TerminatorEvals>' 
    ## INFO  [13:00:45.502] Evaluating 1 configuration(s) 
    ## INFO  [13:00:46.336] Benchmark with 5 resampling iterations 
    ## INFO  [13:00:46.426] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 1/5) 
    ## INFO  [13:00:46.764] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 2/5) 
    ## INFO  [13:00:47.208] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 3/5) 
    ## INFO  [13:00:47.533] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 4/5) 
    ## INFO  [13:00:47.849] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 5/5) 
    ## INFO  [13:00:48.183] Finished benchmark 
    ## INFO  [13:00:48.230] Result of batch 1: 
    ## INFO  [13:00:48.233]  branch.selection anova.filter.frac lrn_branch.selection rf.mtry rpt.cp knn.k 
    ## INFO  [13:00:48.233]               pca         0.1914038                  knn      NA     NA     1 
    ## INFO  [13:00:48.233]  knn.distance regr.rmse                                uhash 
    ## INFO  [13:00:48.233]       1.10569  178.0445 ae0fd23c-7e21-403d-8b2b-a20bf07840b9 
    ## INFO  [13:00:48.281] Evaluating 1 configuration(s) 
    ## INFO  [13:00:49.027] Benchmark with 5 resampling iterations 
    ## INFO  [13:00:49.029] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 1/5) 
    ## INFO  [13:00:49.334] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 2/5) 
    ## INFO  [13:00:49.569] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 3/5) 
    ## INFO  [13:00:49.805] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 4/5) 
    ## INFO  [13:00:50.051] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 5/5) 
    ## INFO  [13:00:50.290] Finished benchmark 
    ## INFO  [13:00:50.347] Result of batch 2: 
    ## INFO  [13:00:50.351]  branch.selection anova.filter.frac lrn_branch.selection rf.mtry    rpt.cp 
    ## INFO  [13:00:50.351]           nothing         0.2102727                  rpt      NA 0.6115479 
    ## INFO  [13:00:50.351]  knn.k knn.distance regr.rmse                                uhash 
    ## INFO  [13:00:50.351]     NA           NA  239.3805 3b03ed74-ddd8-4d0c-a364-2beed6de192d 
    ## INFO  [13:00:50.398] Evaluating 1 configuration(s) 
    ## INFO  [13:00:51.195] Benchmark with 5 resampling iterations 
    ## INFO  [13:00:51.197] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 1/5) 
    ## INFO  [13:00:51.439] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 2/5) 
    ## INFO  [13:00:51.690] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 3/5) 
    ## INFO  [13:00:51.931] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 4/5) 
    ## INFO  [13:00:52.182] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 5/5) 
    ## INFO  [13:00:52.429] Finished benchmark 
    ## INFO  [13:00:52.456] Result of batch 3: 
    ## INFO  [13:00:52.459]  branch.selection anova.filter.frac lrn_branch.selection rf.mtry rpt.cp knn.k 
    ## INFO  [13:00:52.459]           nothing         0.1688675                  knn      NA     NA     1 
    ## INFO  [13:00:52.459]  knn.distance regr.rmse                                uhash 
    ## INFO  [13:00:52.459]      1.002099  106.7945 4185fd1b-e7ac-408e-8434-e6c7f7d03c3a 
    ## INFO  [13:00:52.517] Evaluating 1 configuration(s) 
    ## INFO  [13:00:53.258] Benchmark with 5 resampling iterations 
    ## INFO  [13:00:53.260] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 1/5) 
    ## INFO  [13:00:53.580] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 2/5) 
    ## INFO  [13:00:53.910] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 3/5) 
    ## INFO  [13:00:54.266] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 4/5) 
    ## INFO  [13:00:54.581] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 5/5) 
    ## INFO  [13:00:54.901] Finished benchmark 
    ## INFO  [13:00:54.925] Result of batch 4: 
    ## INFO  [13:00:54.929]  branch.selection anova.filter.frac lrn_branch.selection rf.mtry    rpt.cp 
    ## INFO  [13:00:54.929]               pca         0.4684684                  rpt      NA 0.4318594 
    ## INFO  [13:00:54.929]  knn.k knn.distance regr.rmse                                uhash 
    ## INFO  [13:00:54.929]     NA           NA  343.3437 659b1ab8-d432-436e-a3e9-81b959fe7d08 
    ## INFO  [13:00:54.974] Evaluating 1 configuration(s) 
    ## INFO  [13:00:55.689] Benchmark with 5 resampling iterations 
    ## INFO  [13:00:55.691] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 1/5) 
    ## INFO  [13:00:55.945] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 2/5) 
    ## INFO  [13:00:56.191] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 3/5) 
    ## INFO  [13:00:56.450] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 4/5) 
    ## INFO  [13:00:56.694] Applying learner 'branch.pca.nop.unbranch.anova.lrn_branch.knn.rpt.rf.lrn_unbranch' on task 'meuse' (iter 5/5) 
    ## INFO  [13:00:56.985] Finished benchmark 
    ## INFO  [13:00:57.014] Result of batch 5: 
    ## INFO  [13:00:57.017]  branch.selection anova.filter.frac lrn_branch.selection rf.mtry rpt.cp knn.k 
    ## INFO  [13:00:57.017]           nothing         0.5996147                   rf       2     NA    NA 
    ## INFO  [13:00:57.017]  knn.distance regr.rmse                                uhash 
    ## INFO  [13:00:57.017]            NA  96.38542 0df9da97-ce00-43bd-89b7-e528a686fa03 
    ## INFO  [13:00:57.176] Finished optimizing after 5 evaluation(s) 
    ## INFO  [13:00:57.177] Result: 
    ## INFO  [13:00:57.180]  branch.selection anova.filter.frac lrn_branch.selection rf.mtry rpt.cp knn.k 
    ## INFO  [13:00:57.180]           nothing         0.5996147                   rf       2     NA    NA 
    ## INFO  [13:00:57.180]  knn.distance learner_param_vals  x_domain regr.rmse 
    ## INFO  [13:00:57.180]            NA          <list[5]> <list[4]>  96.38542

    ##    branch.selection anova.filter.frac lrn_branch.selection rf.mtry rpt.cp knn.k
    ## 1:          nothing         0.5996147                   rf       2     NA    NA
    ##    knn.distance learner_param_vals  x_domain regr.rmse
    ## 1:           NA          <list[5]> <list[4]>  96.38542

we create graph to compare how anova performed against rmse

    arx = inst$archive$data()
    ggplot(arx, aes(x = anova.filter.frac, y = regr.rmse)) + geom_line()

![](mlr3_files/figure-markdown_strict/unnamed-chunk-11-1.png)

#### Spatiotemporal CV

This package extends the mlr3 package framework with spatiotemporal
resampling and visualization methods.

    library(mlr3)
    library(mlr3spatiotempcv)
    names(meuse)

    ##  [1] "x"       "y"       "cadmium" "copper"  "lead"    "zinc"    "elev"   
    ##  [8] "dist"    "om"      "ffreq"   "soil"    "lime"    "landuse" "dist.m" 
    ## [15] "lat"     "long"

    meuse$landuse <- NULL
    meuse$om <- NULL
    data = mlr3::as_data_backend(meuse)
    task_meuse = TaskRegrST$new(id = "meuse", backend = data, target = "zinc",
                                positive = "TRUE", coordinate_names = c("x", "y"),crs = CRS("+init=epsg:28992"))

    task_meuse = task_meuse$droplevels()
    learner = lrn("regr.ranger", predict_type = "response")

#### Normal CV

    learner = lrn("regr.ranger", predict_type = "response")
    resampling_nsp = rsmp("repeated_cv", folds = 5, repeats = 5)
    rr_nsp = resample(task = task_meuse, learner = learner,
                      resampling = resampling_nsp)

    ## INFO  [13:00:57.759] Applying learner 'regr.ranger' on task 'meuse' (iter 1/25) 
    ## INFO  [13:00:57.799] Applying learner 'regr.ranger' on task 'meuse' (iter 2/25) 
    ## INFO  [13:00:57.837] Applying learner 'regr.ranger' on task 'meuse' (iter 3/25) 
    ## INFO  [13:00:57.876] Applying learner 'regr.ranger' on task 'meuse' (iter 4/25) 
    ## INFO  [13:00:57.916] Applying learner 'regr.ranger' on task 'meuse' (iter 5/25) 
    ## INFO  [13:00:57.956] Applying learner 'regr.ranger' on task 'meuse' (iter 6/25) 
    ## INFO  [13:00:58.001] Applying learner 'regr.ranger' on task 'meuse' (iter 7/25) 
    ## INFO  [13:00:58.042] Applying learner 'regr.ranger' on task 'meuse' (iter 8/25) 
    ## INFO  [13:00:58.111] Applying learner 'regr.ranger' on task 'meuse' (iter 9/25) 
    ## INFO  [13:00:58.153] Applying learner 'regr.ranger' on task 'meuse' (iter 10/25) 
    ## INFO  [13:00:58.197] Applying learner 'regr.ranger' on task 'meuse' (iter 11/25) 
    ## INFO  [13:00:58.235] Applying learner 'regr.ranger' on task 'meuse' (iter 12/25) 
    ## INFO  [13:00:58.276] Applying learner 'regr.ranger' on task 'meuse' (iter 13/25) 
    ## INFO  [13:00:58.318] Applying learner 'regr.ranger' on task 'meuse' (iter 14/25) 
    ## INFO  [13:00:58.358] Applying learner 'regr.ranger' on task 'meuse' (iter 15/25) 
    ## INFO  [13:00:58.398] Applying learner 'regr.ranger' on task 'meuse' (iter 16/25) 
    ## INFO  [13:00:58.440] Applying learner 'regr.ranger' on task 'meuse' (iter 17/25) 
    ## INFO  [13:00:58.482] Applying learner 'regr.ranger' on task 'meuse' (iter 18/25) 
    ## INFO  [13:00:58.522] Applying learner 'regr.ranger' on task 'meuse' (iter 19/25) 
    ## INFO  [13:00:58.561] Applying learner 'regr.ranger' on task 'meuse' (iter 20/25) 
    ## INFO  [13:00:58.602] Applying learner 'regr.ranger' on task 'meuse' (iter 21/25) 
    ## INFO  [13:00:58.643] Applying learner 'regr.ranger' on task 'meuse' (iter 22/25) 
    ## INFO  [13:00:58.683] Applying learner 'regr.ranger' on task 'meuse' (iter 23/25) 
    ## INFO  [13:00:58.729] Applying learner 'regr.ranger' on task 'meuse' (iter 24/25) 
    ## INFO  [13:00:58.783] Applying learner 'regr.ranger' on task 'meuse' (iter 25/25)

    rr_nsp$aggregate(measures = msr("regr.rmse"))

    ## regr.rmse 
    ##   84.5152

    rr_nsp$aggregate(measures = msr("regr.rsq"))

    ##  regr.rsq 
    ## 0.9416101

#### spatial CV

    resampling_sp = rsmp("repeated-spcv-coords", folds = 5, repeats = 5)
    rr_sp = resample(
      task = task_meuse, learner = learner,
      resampling = resampling_sp)

    ## INFO  [13:00:58.934] Applying learner 'regr.ranger' on task 'meuse' (iter 1/25) 
    ## INFO  [13:00:58.973] Applying learner 'regr.ranger' on task 'meuse' (iter 2/25) 
    ## INFO  [13:00:59.023] Applying learner 'regr.ranger' on task 'meuse' (iter 3/25) 
    ## INFO  [13:00:59.060] Applying learner 'regr.ranger' on task 'meuse' (iter 4/25) 
    ## INFO  [13:00:59.105] Applying learner 'regr.ranger' on task 'meuse' (iter 5/25) 
    ## INFO  [13:00:59.148] Applying learner 'regr.ranger' on task 'meuse' (iter 6/25) 
    ## INFO  [13:00:59.182] Applying learner 'regr.ranger' on task 'meuse' (iter 7/25) 
    ## INFO  [13:00:59.220] Applying learner 'regr.ranger' on task 'meuse' (iter 8/25) 
    ## INFO  [13:00:59.261] Applying learner 'regr.ranger' on task 'meuse' (iter 9/25) 
    ## INFO  [13:00:59.300] Applying learner 'regr.ranger' on task 'meuse' (iter 10/25) 
    ## INFO  [13:00:59.336] Applying learner 'regr.ranger' on task 'meuse' (iter 11/25) 
    ## INFO  [13:00:59.387] Applying learner 'regr.ranger' on task 'meuse' (iter 12/25) 
    ## INFO  [13:00:59.425] Applying learner 'regr.ranger' on task 'meuse' (iter 13/25) 
    ## INFO  [13:00:59.464] Applying learner 'regr.ranger' on task 'meuse' (iter 14/25) 
    ## INFO  [13:00:59.504] Applying learner 'regr.ranger' on task 'meuse' (iter 15/25) 
    ## INFO  [13:00:59.543] Applying learner 'regr.ranger' on task 'meuse' (iter 16/25) 
    ## INFO  [13:00:59.581] Applying learner 'regr.ranger' on task 'meuse' (iter 17/25) 
    ## INFO  [13:00:59.620] Applying learner 'regr.ranger' on task 'meuse' (iter 18/25) 
    ## INFO  [13:00:59.662] Applying learner 'regr.ranger' on task 'meuse' (iter 19/25) 
    ## INFO  [13:00:59.700] Applying learner 'regr.ranger' on task 'meuse' (iter 20/25) 
    ## INFO  [13:00:59.739] Applying learner 'regr.ranger' on task 'meuse' (iter 21/25) 
    ## INFO  [13:00:59.784] Applying learner 'regr.ranger' on task 'meuse' (iter 22/25) 
    ## INFO  [13:00:59.822] Applying learner 'regr.ranger' on task 'meuse' (iter 23/25) 
    ## INFO  [13:00:59.868] Applying learner 'regr.ranger' on task 'meuse' (iter 24/25) 
    ## INFO  [13:00:59.910] Applying learner 'regr.ranger' on task 'meuse' (iter 25/25)

    rr_sp$aggregate(measures = msr("regr.rmse"))

    ## regr.rmse 
    ##  105.6852

    rr_sp$aggregate(measures = msr("regr.rsq"))

    ##  regr.rsq 
    ## 0.9048309

As we see rsq for spatial CV is lower than rsq for normal CV and
considering the autocorrelation effect result of spatial CV is less
biased than normal CV.

Visualization of data in blocks

    autoplot(object = resampling_sp, task = task_meuse)

    ## Warning in CPL_crs_from_input(x): GDAL Message 1: +init=epsg:XXXX syntax is
    ## deprecated. It might return a CRS with a non-EPSG compliant axis order.

![](mlr3_files/figure-markdown_strict/unnamed-chunk-15-1.png)
